<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use FOS\UserBundle\Model\User as BaseUser;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Exclude;
use JMS\Serializer\Annotation\Expose;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 * @ExclusionPolicy("all")
 */
class User extends BaseUser
{
    use TraitEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    protected $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    protected $updatedAt;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="Product", mappedBy="createdby")
     */
    private $createdProducts;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="Product", mappedBy="modifiedby")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    private $modifiedProducts;

     /**
      * Constructor
      */
     public function __construct()
     {
         $this->createdProducts = new \Doctrine\Common\Collections\ArrayCollection();

     }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Add createdProduct
     *
     * @param \AppBundle\Entity\Product $createdProduct
     *
     * @return User
     */
    public function addCreatedProduct(\AppBundle\Entity\Product $createdProduct)
    {
        $this->createdProducts[] = $createdProduct;

        return $this;
    }

    /**
     * Remove createdProduct
     *
     * @param \AppBundle\Entity\Product $createdProduct
     */
    public function removeCreatedProduct(\AppBundle\Entity\Product $createdProduct)
    {
        $this->createdProducts->removeElement($createdProduct);
    }

    /**
     * Get createdProducts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCreatedProducts()
    {
        return $this->createdProducts;
    }

    /**
     * Add modifiedProduct
     *
     * @param \AppBundle\Entity\Product $modifiedProduct
     *
     * @return User
     */
    public function addModifiedProduct(\AppBundle\Entity\Product $modifiedProduct)
    {
        $this->modifiedProducts[] = $modifiedProduct;

        return $this;
    }

    /**
     * Remove modifiedProduct
     *
     * @param \AppBundle\Entity\Product $modifiedProduct
     */
    public function removeModifiedProduct(\AppBundle\Entity\Product $modifiedProduct)
    {
        $this->modifiedProducts->removeElement($modifiedProduct);
    }

    /**
     * Get modifiedProducts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getModifiedProducts()
    {
        return $this->modifiedProducts;
    }

    /**
     * @inheritDoc
     */
    public function getRoles()
    {
        return ["ROLE_USER"];
    }
}
