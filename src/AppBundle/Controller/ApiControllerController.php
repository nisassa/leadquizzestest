<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Entity\Product;
use AppBundle\Entity\User;
use AppBundle\Entity\Category;

class ApiControllerController extends Controller
{
    /**
     * @Route("/api/list/products")
     */
    public function listAllAction()
    {
        $products = $this->getDoctrine()->getRepository(Product::class)->findAll();
        if (! $products) {
            return new JsonResponse(["message" => "Not found!"], Response::HTTP_NOT_FOUND);
        }
        $data = $this->get('jms_serializer')->serialize($products, 'json');
        return new JsonResponse($data, 200);
    }

    /**
     * @Route("/api/list/categories")
     */
    public function listAllCategoriesAction()
    {
        $categories = $this->getDoctrine()->getRepository(Category::class)->findAll();
        if (! $categories) {
            return new JsonResponse(["message" => "Not found!"], Response::HTTP_NOT_FOUND);
        }
        $data = $this->get('jms_serializer')->serialize($categories, 'json');
        return new JsonResponse($data, 200);
    }

    /**
     * @Route("/api/product/{id}")
     */
    public function retrieveProductAction($id)
    {
        $product = $this->getDoctrine()->getRepository(Product::class)->find($id);
        if (! $product) {
            return new JsonResponse(["message" => "Not found!"], Response::HTTP_NOT_FOUND);
        }
        $data = $this->get('jms_serializer')->serialize($product, 'json');
        return new JsonResponse($data, 200);
    }

    /**
     * @Route("/api/update/product/{id}")
     */
    public function updateProductAction(Request $request, $id)
    {
        $user = $this->getUser();
        if (! $user) {
            return new JsonResponse(["message" => "Bad Request!"], Response::HTTP_BAD_REQUEST);
        }
        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository(Product::class)->find($id);
        if (! $product) {
            return new JsonResponse(["message" => "Not found!"], Response::HTTP_NOT_FOUND);
        }
        $data = json_decode($request->getContent(), true);

        if (! empty($data['name'])) {
            $product->setname((string)$data['name']);
        }
        if (! empty($data['sku'])) {
            $product->setSku((string)$data['sku']);
        }
        if (! empty($data['price'])) {
            $product->setPrice((float)$data['price']);
        }
        if (! empty($data['quantity'])) {
            $product->setQuantity((int)$data['quantity']);
        }
        if (! empty($data['category'])) {
            $category = $em->getRepository(Category::class)->findOneby(['name' => $data['category']]);
            if ($category) {
                $product->setCategory($category);
            }
        }
        $product->setModifiedby($user);
        $em->persist($product);
        $em->flush();

        return new JsonResponse(["message" => "OK"], 200);
    }

    /**
     * @Route("/api/delete/product/{id}")
     */
    public function deleteProductAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository(Product::class)->find($id);
        if (! $product) {
            return new JsonResponse(["message" => "Not found!"], Response::HTTP_NOT_FOUND);
        }
        $em->remove($product);
        $em->flush($product);
        return new JsonResponse(["message" => "OK"], 200);
    }

    /**
     * @Route("/api/create/product")
     */
    public function createProductAction(Request $request)
    {
        $user = $this->getUser();
        if (! $user) {
            return new JsonResponse(["message" => "Bad Request!"], Response::HTTP_BAD_REQUEST);
        }
        $em = $this->getDoctrine()->getManager();
        $data = json_decode($request->getContent(), true);

        // not the best validation in the world :XX..
        // Normaly I' will create a validation service where I will check each parameter and return an error message
        if (empty($data['name'])
            or empty($data['sku'])
            or empty($data['price'])
            or empty($data['name'])
            or empty($data['quantity'])
            or empty($data['category'])) {
            return new JsonResponse(["message" => "Missing Parameters!"], Response::HTTP_BAD_REQUEST);
        }

        try {
            $product = new Product();
            $product->setname((string)$data['name']);
            $product->setSku((string)$data['sku']);
            $product->setPrice((float)$data['price']);
            $product->setQuantity((int)$data['quantity']);
            $product->setCreatedby($user);

            $category = $em->getRepository(Category::class)->findOneby(['name' => $data['category']]);
            if ($category) {
                $product->setCategory($category);
            }
            $em->persist($product);
            $em->flush();
        } catch (\Exception $e){
            throw new \Exception("Failed to create new product. {$e->getMessage()}");
        }
        return new JsonResponse(["message" => "OK"], 200);
    }

}
