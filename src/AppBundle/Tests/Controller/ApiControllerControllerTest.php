<?php

namespace AppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ApiControllerControllerTest extends WebTestCase
{
    public function testLogin()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/api/login');
    }

    public function testListall()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/api/list/products');
    }

    public function testListallcategories()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/api/list/categories');
    }

    public function testRetrieveproduct()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/api/product');
    }

    public function testUpdateproduct()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/api/product/update');
    }

    public function testDeleteproduct()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/api/product/delete');
    }

}
