<?php

namespace AppBundle\Service;

use AppBundle\Entity\Product;
use AppBundle\Entity\Category;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;

/**
 * DataImporter
 */
class DataImporter
{
    CONST API_URL = "https://lq3-production.s3-us-west-2.amazonaws.com/coding-challenge/data-fixtures.json";

    /**
      * @var EncoderFactoryInterface
      */
    private $encoder;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var EntityManagerInterface
     */
     private $m;

    /**
     * Constructor
     * @var EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em, ContainerInterface $container, EncoderFactoryInterface $encoder)
    {
        $this->em = $em;
        $this->container = $container;
        $this->encoder = $encoder;
    }

    /**
     * @return object
     */
    public function fetchData()
    {
        $json = file_get_contents(self::API_URL);
        $result = json_decode($json);
        return $result;
    }

    /**
     * @var array $products
     * @return null
     */
    public function saveUsers(array $users)
    {
        $userRepo = $this->em->getRepository(User::class);
        $adminEmail = $this->container->getParameter('admin_email');
        $adminUser = $this->em->getRepository(User::class)->findOneby(["email" => $adminEmail]);

        foreach ($users as $u) {
            $username = $this->getRandomUsername($u->name);
            $user = new User();
            $user
                ->setEmail($u->email)
                ->setName($u->name)
                ->setUserName($username)
                ->setEnabled(false)
            ;

            // hash the random password
            $encoder = $this->getEncoder($user);
            $plainPassword = $this->randomPassword();
            $encoded = $encoder->encodePassword($plainPassword, 15);

            $user->setPassword($encoded);

            $this->em->persist($user);
            $this->em->flush();
        }
    }

    /**
     * @var array $products
     * @return null
     */
    public function saveProducts(array $products)
    {
        $categoryRepo = $this->em->getRepository(Category::class);
        $adminEmail = $this->container->getParameter('admin_email');
        $adminUser = $this->em->getRepository(User::class)->findOneby(["email" => $adminEmail]);

        foreach ($products as $p) {
            $product = new Product();
            $product
                ->setName($p->name)
                ->setSku($p->sku)
                ->setPrice($p->price)
                ->setQuantity($p->quantity)
                ->setCreatedBy($adminUser);
            ;
            $category = $categoryRepo->findOneby(['name' => $p->category]);
            if (! $category) {
                $category = new Category();
                $category->setName($p->category);
                $this->em->persist($category);
            }
            $product->setCategory($category);
            $this->em->persist($product);
            $this->em->flush();
        }
    }

    /**
     * @var string $string
     * @return string $username
     */
    private function getRandomUsername($string) {
        $pattern = " ";
        $firstPart = strstr(strtolower($string), $pattern, true);
        $secondPart = substr(strstr(strtolower($string), $pattern, false), 0,3);
        $nrRand = rand(0, 100);
        $username = trim($firstPart).trim($secondPart).trim($nrRand);
        return $username;
    }

    /**
     * @return string $pass
     */
    private function randomPassword() {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array();
        $alphaLength = strlen($alphabet) - 1;
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass);
    }


    /**
     * @param User $user
     * @return mixed
     */
    protected function getEncoder(User $user)
    {
        return $this->encoder->getEncoder($user);
    }
}
