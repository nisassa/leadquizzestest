<?php

namespace AppBundle\DataFixtures;

use AppBundle\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;


class UsersFixtures extends Fixture
{
    /**
      * @var EncoderFactoryInterface
      */
    private $encoder;

    /**
      * Constructor
      */
    public function __construct(EncoderFactoryInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    /**
      * {@inheritdoc}
      */
    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setName('Dumitru Plamadeala');
        $user->setUserName('dumitru');
        $user->setEmail('dyma131993@gmail.com');
        $user->setEnabled(true);

        $encoder = $this->getEncoder($user);
        $plainPassword = 'Dumitrupass';
        $encoded = $encoder->encodePassword($plainPassword, 15);
        $user->setPassword($encoded);

        $manager->persist($user);
        $manager->flush();
    }

    /**
     * @param User $user
     * @return mixed
     */
    protected function getEncoder(User $user)
    {
        return $this->encoder->getEncoder($user);
    }
}
