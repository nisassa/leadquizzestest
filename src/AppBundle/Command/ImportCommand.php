<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use AppBundle\Service\DataImporter;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * ImportCommand
 */
class ImportCommand extends ContainerAwareCommand
{
    /**
     * @var DataImporter
     */
    private $dataImporter;

    /**
     * Constructor
     * @var DataImporter $dataImporter
     */
    public function __construct(DataImporter  $dataImporter)
    {
        $this->dataImporter = $dataImporter;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('app:import:data')
            ->setDescription('Parses data from datta fixtures API endpoint.')
        ;
    }

    /**
     * Read each one of the phones on the contactItemsList and parse them.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $output->writeln('Fetching data..');
        $data = $this->dataImporter->fetchData();

        $products = $data->products;
        if (! empty($products)) {
            $this->dataImporter->saveProducts($products);
        }

        $users = $data->users;
        if (! empty($users)) {
            $this->dataImporter->saveUsers($users);
        }

        $output->writeln('Done. Data imported.');
    }
}
